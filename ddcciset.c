#include <fcntl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <unistd.h>

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LOG_ERROR	stderr
#define LOG_MESSAGE	stdout

int i2c_send
(
	const int fd,
	void * data,
	const size_t size
)
{
	struct i2c_msg msg = {
		.addr	= 0x37,
		.flags	= 0x00,
		.len	= size,
		.buf	= data
	};
#ifdef LOG_MESSAGE // {
	fprintf(LOG_MESSAGE, "i2c_send(.addr=%02X, .flags=%02X, .len=%zu, .buf=", msg.addr, msg.flags, size);
	for(size_t i = 0; i < size-1; ++i)
	{
		fprintf(LOG_MESSAGE, "%02X ", ((uint8_t *) data)[i]);
	}
	fprintf(LOG_MESSAGE, "%02X)\n", ((uint8_t *) data)[size-1]);
#endif // LOG_MESSAGE }
	struct i2c_rdwr_ioctl_data rdwr_ioctl_data = {
		.msgs = &msg,
		.nmsgs = 1
	};
	return ioctl(fd, I2C_RDWR, &rdwr_ioctl_data);
}

int ddc_send
(
	const int fd,
	const void * data,
	const size_t size
)
{
	if(size > 0x7F)
	{
#ifdef LOG_ERROR // {
		fprintf(LOG_ERROR, "error: %i: size > 0x7F\n", __LINE__);
#endif // LOG_ERROR }
		return -1;
	}
	uint8_t _data[1+1+size+1];
	_data[0] = 0x51;
	_data[1] = 0x80 | (size & 0x7F);
	memcpy(_data+2, data, size);
	_data[sizeof(_data)-1] = 0x6E;
	for(size_t i = 0; i < sizeof(_data)-1; ++i)
	{
		_data[sizeof(_data)-1] ^= _data[i];
	}
	return i2c_send(fd, _data, sizeof(_data));
}

int set
(
	const int fd,
	const uint8_t index,
	const uint16_t value
)
{
	const uint8_t data[] = {
		0x03,
		index,
		value>>8,
		value & 0xFF
	};
	return ddc_send(fd, data, sizeof(data));
}

int _atoi
(const char * s)
{
	return strtol(s, NULL, s[0] == '0' ? s[1] == 'x' ? 16 : 8 : 10);
}

int main
(
	int argc,
	char * argv[]
)
{
	if(argc < 1+3)
	{
		printf("%s",
			"ddcciset"
			"\n"
			"\n"
			"Usage:\n"
			"  ddcciset device index value\n"
		);
		return 1;
	}
	const char * device = argv[1];
	const int index = _atoi(argv[2]);
	if(index < 0 || index > UINT8_MAX)
	{
#ifdef LOG_ERROR // {
		fprintf(LOG_ERROR, "error: %i: index < 0 || index > UINT8_MAX\n", __LINE__);
#endif // LOG_ERROR }
		return 1;
	}
	const int value = _atoi(argv[3]);
	if(value < 0 || value > UINT16_MAX)
	{
#ifdef LOG_ERROR // {
		fprintf(LOG_ERROR, "error: %i: value < 0 || value > UINT16_MAX\n", __LINE__);
#endif // LOG_ERROR }
		return 1;
	}
#ifdef LOG_MESSAGE // {
	fprintf(LOG_MESSAGE, "device=%s, index=0x%02X, value=0x%04X\n", device, index, value);
#endif // LOG_MESSAGE }
	const int fd = open(device, O_RDWR);
	if(fd < 0)
	{
#ifdef LOG_ERROR // {
		fprintf(LOG_ERROR, "error: %i: open < 0\n", __LINE__);
#endif // LOG_ERROR }
		return 1;
	}
	if(set(fd, index, value) < 0)
	{
#ifdef LOG_ERROR // {
		fprintf(LOG_ERROR, "error: %i: set < 0\n", __LINE__);
#endif // LOG_ERROR }
		close(fd);
		return 1;
	}
	close(fd);
	return 0;
}
