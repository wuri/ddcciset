.PHONY: all install

all: ddcciset.c
	$(CC) $(CFLAGS) -std=c99 ddcciset.c -o ddcciset

install: ddcciset
	mv ddcciset $(DESTDIR)/ddcciset
